# OOP Design Patterns and SOLID Principles Examples

This repository contains code examples demonstrating Object-Oriented Design Patterns and SOLID Principles. The examples are implemented in four Python files, each focusing on one of the most commonly used design patterns.

## Files

- `abstraction_example.py`: Demonstrates an example of the Abstraction design pattern.
- `factory_example.py`: Provides an implementation of the Factory design pattern.
- `strategy_example.py`:  Demonstrates an example of the Strategy design pattern in Python.
- `facade_example.py`: Provides an implementation of the Facade design pattern. Strategy design pattern is also used.

## Supplementary Files

The `commons/enums` directory contains enumerations used in the main examples.

The `extras` directory includes supplementary classes or components needed for the four primary files.

Additional files located in the root directory:
- `object_constructor.py`: A utility module used in `facade_example.py`.
- `http_request_body.json`: A JSON file representing the HTTP request body used in `facade_example.py`.

Please note that the code in the supplementary files serves as illustrations and may not represent fully functional code.

## Author

Nikolaos Galanakis
