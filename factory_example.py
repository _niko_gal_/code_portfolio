from abc import abstractmethod, ABC
from extras.enums import ConnectionLocation
from extras.model import AssemblyConnector, Node, Part, IpWall, OopWall, FlatSlider, BeamDiaphragm


class ConnectorFactory(ABC):

    @abstractmethod
    def get_connector_node_i(
        self,
        connection_location: ConnectionLocation
    ) -> Node:
        """returns the node i of the connector"""

    @abstractmethod
    def get_connector_node_j(
        self,
        connection_location: ConnectionLocation
    ) -> Node:
        """returns the node j of the connector"""

    @abstractmethod
    def get_connector(
        self,
        connection_location: ConnectionLocation,
        parent_part: Part,
        connecting_part: Part
    ) -> AssemblyConnector:
        """returns connector between the parent part and the connecting part."""


class OopWallFlatSliderConnector(ConnectorFactory):

    def get_connector_node_i(
        self,
        connection_location: ConnectionLocation,
        parent_part: Part,
        connecting_part: Part
    ) -> Node:

        node_i_map = {
            ConnectionLocation.BOT_FACE_OOP_WALL: connecting_part.node_j,
            ConnectionLocation.TOP_FACE_OOP_WALL: parent_part.node_j,
        }
        if connection_location not in node_i_map.keys():
            raise Exception(f"Location {connection_location.location} is not appropriate connection location.")

        return node_i_map[connection_location]

    def get_connector_node_j(
        self,
        connection_location: ConnectionLocation,
        parent_part: Part,
        connecting_part: Part
    ) -> Node:

        node_j_map = {
            ConnectionLocation.BOT_FACE_OOP_WALL: parent_part.node_i,
            ConnectionLocation.TOP_FACE_OOP_WALL: connecting_part.node_i,
        }
        if connection_location not in node_j_map.keys():
            raise Exception(f"Location {connection_location.location} is not appropriate connection location.")

        return node_j_map[connection_location]

    def get_connector(
        self,
        connection_location: ConnectionLocation,
        parent_part: Part,
        connecting_part: Part
    ) -> AssemblyConnector:

        return AssemblyConnector(
            parent_part=parent_part,
            connecting_part=connecting_part,
            node_i=self.get_connector_node_i(parent_part, connection_location),
            node_j=self.get_connector_node_j(connecting_part, connection_location),
            connection_location=connection_location
        )


class IpWallDiaConnector(ConnectorFactory):

    def get_connector_node_i(
        self,
        connection_location: ConnectionLocation,
        parent_part: Part
    ) -> Node:

        node_i_map = {
            ConnectionLocation.DIA_EDGE_NODE_I: parent_part.node_j,
            ConnectionLocation.DIA_EDGE_NODE_J: parent_part.node_j
        }
        if connection_location not in node_i_map.keys():
            raise Exception(f"Location {connection_location.location} is not appropriate connection location.")

        return node_i_map[connection_location]

    def get_connector_node_j(
        self,
        connection_location: ConnectionLocation,
        connecting_part: Part
    ) -> Node:

        node_j_map = {
            ConnectionLocation.DIA_EDGE_NODE_I: connecting_part.node_i,
            ConnectionLocation.DIA_EDGE_NODE_J: connecting_part.node_j
        }
        if connection_location not in node_j_map.keys():
            raise Exception(f"Location {connection_location.location} is not appropriate connection location.")

        return node_j_map[connection_location]

    def get_connector(
        self,
        connection_location: ConnectionLocation,
        parent_part: Part,
        connecting_part: Part
    ) -> AssemblyConnector:

        return AssemblyConnector(
            parent_part=parent_part,
            connecting_part=connecting_part,
            node_i=self.get_connector_node_i(connection_location, parent_part),
            node_j=self.get_connector_node_j(connection_location, connecting_part),
            connection_location=connection_location
        )


def get_connector_factory(parent_part: Part, connecting_part: Part) -> ConnectorFactory:

    constructor_map = {
        (OopWall, FlatSlider): OopWallFlatSliderConnector(),
        (IpWall, BeamDiaphragm): IpWallDiaConnector(),
    }

    key = (
        parent_part.__class__,
        connecting_part.__class__
    )

    if key not in constructor_map.keys():
        raise Exception(f"Assembly Connector {key} does not belong in the constructor map.")

    return constructor_map[key]


def collect_connector(
    connector_factory: ConnectorFactory,
    parent_part: Part,
    connecting_part: Part,
    connection_location: ConnectionLocation
) -> AssemblyConnector:
    return connector_factory.get_connector(
        parent_part=parent_part,
        connecting_part=connecting_part,
        connection_location=connection_location
    )


if __name__ == "__main__":
    parent_part = OopWall()
    connecting_part = BeamDiaphragm()
    connection_location = ConnectionLocation.DIA_EDGE_NODE_J.location
    connector_factory = get_connector_factory(parent_part, connecting_part)
    connector = collect_connector(
        connector_factory=connector_factory,
        parent_part=parent_part,
        connecting_part=connecting_part,
        connection_location=connection_location
    )
