from extras.model import CrossSection
from extras.engine import Shrinkage, Creep, NominalBetonCover, AnalysisSettings 
from abstraction_example import GeometryShape, Rectangle


class ObjectContainer:

    def __init__(self, http_request_body: dict) -> None:
        self.http_request_body = http_request_body

        self.creeps = self.construct_creeps()
        self.shrinkages = self.construct_shrinkages()
        self.concrete_covers = self.construct_concrete_covers()
        self.cross_sections = self.construct_cross_sections()
        self.analysis_settings = self.construct_analysis_settings()

    def construct_creep(self, creep_dict: dict[str, float]) -> Creep:
        return Creep(
            ambient_relative_humidity=creep_dict['ambientRelativeHumidity'],
            ambient_temperature=creep_dict['ambientTemperature'],
            time_of_creep_deformation=creep_dict['creepDeformationTime']        
        )

    def construct_shrinkage(self, shrinkage_dict: dict[str, float]) -> Shrinkage:
        return Shrinkage(
            ambient_relative_humidity=shrinkage_dict['ambientRelativeHumidity'],
            concrete_age_at_curing_end=shrinkage_dict['concreteAtCuringEnd'],
            current_concrete_age=shrinkage_dict['currentConcreteAge']
        )

    def construct_concrete_cover(self, concrete_cover_dict: dict[str, float | str]) -> NominalBetonCover:
        return NominalBetonCover(
            bar_diameter=concrete_cover_dict['barDiameter'],
            casting_method=concrete_cover_dict['castingMethod'],
            exposure_class=concrete_cover_dict['exposureClass'],
            structural_class=concrete_cover_dict['structuralClass']
        )

    def construct_geometry(self, cross_section_dict: dict[str, float | str]) -> GeometryShape:
        geom = cross_section_dict['geometryType']
        if geom == 'Rectangle':
            return Rectangle(width=cross_section_dict['width'], height=cross_section_dict['height'])
        else:
            pass

    def construct_cross_section(self, cross_section_dict: dict[str, float | str]) -> CrossSection:
        return CrossSection(
            geometry_shape=self.construct_geometry(cross_section_dict),
            creep=self.creeps[cross_section_dict['creep']],
            shrinkage=self.shrinkages[cross_section_dict['shrinkage']],
            concrete_cover=self.concrete_covers[cross_section_dict['concreteCover']]
        )

    def construct_creeps(self) -> dict[str, Creep]:
        return {
            key: self.construct_creep(value)
            for key, value in self.http_request_body['creeps'].items()
        }

    def construct_shrinkages(self) -> dict[str, Shrinkage]:
        return {
            key: self.construct_shrinkage(value)
            for key, value in self.http_request_body['shrinkages'].items()
        }

    def construct_concrete_covers(self) -> dict[str, NominalBetonCover]:
        return {
            key: self.construct_concrete_cover(value)
            for key, value in self.http_request_body['concrete_covers'].items()
        }

    def construct_cross_sections(self) -> dict[str, CrossSection]:
        return {
            key: self.construct_cross_section(value)
            for key, value in self.http_request_body['cross_sections'].items()
        }

    def construct_analysis_settings(self) -> AnalysisSettings:
        analysis_settings_dict = self.http_request_body["analysis_settings"]
        return AnalysisSettings(
            fiber_height=analysis_settings_dict["fiber_height"],
            equilibrium_tolerance=analysis_settings_dict["equilibrium_tolerance"],
            maximum_reinforcement_strain=analysis_settings_dict["maximum_reinforcement_strain"],
            reinforcement_strain_step=analysis_settings_dict["reinforcement_strain_step"],
            concrete_strain_step=analysis_settings_dict["concrete_strain_step"]
        )
