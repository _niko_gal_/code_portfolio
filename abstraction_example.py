from abc import abstractmethod, ABC


class GeometryShape(ABC):

    @abstractmethod
    def get_children_geometry_shapes(self) -> float:
        """
        returns the children geometry shapes
        that produce the parent geometry shape
        """

    @abstractmethod
    def get_area(self) -> float:
        """returns the area of the shape"""

    @abstractmethod
    def get_perimeter(self) -> float:
        """returns the perimeter of the shape"""

    @abstractmethod
    def get_neutral_axis_y(self) -> float:
        """returns the y of neutral axis"""

    @abstractmethod
    def get_neutral_axis_z(self) -> float:
        """returns the z of neutral axis"""

    @abstractmethod
    def get_Iy(self) -> float:
        """returns the area moment of inertia y"""

    @abstractmethod
    def get_Iz(self) -> float:
        """returns the area moment of inertia z"""

    @abstractmethod
    def get_area_until_distance_from_top_fiber(
        self,
        distance_from_top_fiber: float
    ) -> float:
        """
        returns the area of the cross section part
        that corresponds until a given distance from
        the top fiber
        """

    @abstractmethod
    def get_area_until_distance_from_bot_fiber(
        self,
        distance_from_bot_fiber: float
    ) -> float:
        """
        returns the area of the cross section part
        that corresponds until a given distance from
        the bot fiber
        """

    @abstractmethod
    def get_neutral_axis_y_until_distance_from_top_fiber(
        self,
        distance_from_top_fiber: float
    ) -> float:
        """
        returns the neutral axis y of the cross section part
        corresponding until the given distance from the top fiber
        """

    @abstractmethod
    def get_neutral_axis_y_until_distance_from_bot_fiber(
        self,
        distance_from_bot_fiber: float
    ) -> float:
        """
        returns the neutral axis y of the cross section part
        corresponding until the given distance from the bot fiber
        """

    @abstractmethod
    def get_width_at_distance_from_top_fiber(
        self,
        distance_from_top_fiber: float
    ) -> float:
        """
        returns the the width of the geometry shape at a
        distance from top fiber
        """

    @abstractmethod
    def check_value_error(self) -> None:
        """checks input and raises ValueError if input is not correct"""


class Rectangle(GeometryShape):

    def __init__(self, width: float, height: float) -> None:
        self._width = width
        self._height = height
        self.check_value_error()
        self._area = self.get_area()
        self._perimeter = self.get_perimeter()
        self._neutral_axis_y = self.get_neutral_axis_y()
        self._neutral_axis_z = self.get_neutral_axis_z()
        self._Iy = self.get_Iy()
        self._Iz = self.get_Iz()
        self._children_geometry_shapes = self.get_children_geometry_shapes()

    def get_children_geometry_shapes(self) -> list[GeometryShape]:
        return []

    def get_area(self) -> float:
        """returns the area of the shape"""
        return self._width*self._height

    def get_perimeter(self) -> float:
        """returns the perimeter of the shape"""
        return 2*(self._width+self._height)

    def get_neutral_axis_y(self) -> float:
        """returns the y of neutral axis"""
        return self._height/2

    def get_neutral_axis_z(self) -> float:
        """returns the z of neutral axis"""
        return self._width/2

    def get_Iy(self) -> float:
        """returns the area moment of inertia Iy"""
        return (1/12)*self._width*self._height**3

    def get_Iz(self) -> float:
        """returns the area moment of inertia Iz"""
        return (1/12)*self._height*self._width**3

    def get_area_until_distance_from_top_fiber(
        self,
        distance_from_top_fiber: float
    ) -> float:
        """
        returns the area of the cross section part
        that corresponds until a given distance from
        the top fiber
        """
        if distance_from_top_fiber < 0:
            raise ValueError(
                "Input for distance from the top fiber cannot be negative."
            )

        if distance_from_top_fiber >= self._total_height:
            area = self._area
        else:
            area = self._width*distance_from_top_fiber

        return area

    def get_area_until_distance_from_bot_fiber(
        self,
        distance_from_bot_fiber: float
    ) -> float:
        """
        returns the area of the cross section part
        that corresponds until a given distance from
        the bot fiber
        """
        if distance_from_bot_fiber < 0:
            raise ValueError(
                "Input for distance from the bot fiber cannot be negative."
            )

        if distance_from_bot_fiber >= self._total_height:
            area = self._area
        else:
            area = self._width*distance_from_bot_fiber

        return area

    def get_neutral_axis_y_until_distance_from_top_fiber(
        self,
        distance_from_top_fiber: float
    ) -> float:
        """
        returns the neutral axis y of the cross section part
        corresponding until the given distance from the top fiber
        """
        if distance_from_top_fiber < 0:
            raise ValueError(
                "Input for distance from the top fiber cannot be negative."
            )
        return distance_from_top_fiber/2

    def get_neutral_axis_y_until_distance_from_bot_fiber(
        self,
        distance_from_bot_fiber: float
    ) -> float:
        """
        returns the neutral axis y of the cross section part
        corresponding until the given distance from the bot fiber
        """
        if distance_from_bot_fiber < 0:
            raise ValueError(
                "Input for distance from the bot fiber cannot be negative."
            )
        return self._total_height-distance_from_bot_fiber/2

    def get_width_at_distance_from_top_fiber(
        self,
        distance_from_top_fiber: float
    ) -> float:
        """
        returns the the width of the geometry shape at a
        distance from top fiber
        """
        return self._total_width

    def check_value_error(self) -> None:
        """checks input and raises ValueError if input is not correct"""
        check_list = [self._width > 0, self._height > 0]
        if False in check_list:
            raise ValueError(
                "Input for rectangular shape cannot be negative or 0."
            )

    @property
    def width(self) -> float:
        return self._width

    @property
    def height(self) -> float:
        return self._height

    @property
    def area(self) -> float:
        return self.area

    @property
    def perimeter(self) -> float:
        return self._perimeter

    @property
    def neutral_axis_y(self) -> float:
        return self._neutral_axis_y

    @property
    def neutral_axis_z(self) -> float:
        return self._neutral_axis_z

    @property
    def Iy(self) -> float:
        return self._Iy

    @property
    def Iz(self) -> float:
        return self._Iz

    @property
    def children_geometry_shapes(self) -> float:
        return self._children_geometry_shapes
