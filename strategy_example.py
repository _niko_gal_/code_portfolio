from abc import abstractmethod, ABC
import numpy as np
from extras.model import CrossSection
from extras.engine import StrainDistribution, AnalysisSettings
from abstraction_example import Rectangle
from commons.enums.enums import CrossSectionLoadingState, StressDistributionType


class StrainDistributionStrategy(ABC):

    @abstractmethod
    def get_strain_distributions(
        self,
        cross_section: CrossSection,
        analysis_settings: AnalysisSettings,
        stress_distribution_type: StressDistributionType
    ) -> list[StrainDistribution]:
        """
        returns a list of strain distributions
        through the height of the cross-section
        based on a strain step given in the analysis
        settings.
        """

    @abstractmethod
    def get_cross_section_loading_state(self) -> CrossSectionLoadingState:
        """
        returns the loading state of the
        cross section
        """


class PureCompression(StrainDistributionStrategy):

    def get_strain_distributions(
        self,
        cross_section: CrossSection,
        analysis_settings: AnalysisSettings,
        stress_distribution_type=StressDistributionType.RECTANGULAR
    ) -> list[StrainDistribution]:

        return [StrainDistribution(
            distribution_height=cross_section.geometry.height,
            e_top_fiber=-cross_section.material.strength_deform_characteristics.ec2,
            e_bot_fiber=-cross_section.material.strength_deform_characteristics.ec2
        )]

    def get_cross_section_loading_state(cls) -> CrossSectionLoadingState:

        return CrossSectionLoadingState.PURE_COMPRESSION


class PureTension(StrainDistributionStrategy):

    def get_strain_distributions(
        self,
        cross_section: CrossSection,
        analysis_settings: AnalysisSettings,
        stress_distribution_type=StressDistributionType.RECTANGULAR
    ) -> list[StrainDistribution]:

        return [StrainDistribution(
            distribution_height=cross_section.geometry.height,
            e_top_fiber=analysis_settings.maximum_reinforcement_strain,
            e_bot_fiber=analysis_settings.maximum_reinforcement_strain
        )]

    def get_cross_section_loading_state(self) -> CrossSectionLoadingState:

        return CrossSectionLoadingState.PURE_TENSION


class PureBendingCompressionOnTopFiber(StrainDistributionStrategy):

    def get_max_strain(
        self,
        cross_section: CrossSection,
        stress_distribution_type: StressDistributionType
    ) -> float:

        e_strain_map = {
            StressDistributionType.PARABOLA_RECTANGLE: -cross_section.material.strength_deform_characteristics.ecu2,
            StressDistributionType.RECTANGULAR: -cross_section.material.strength_deform_characteristics.ecu3,
            StressDistributionType.TRIANGULAR: -cross_section.material.strength_deform_characteristics.ecu2
        }

        return e_strain_map[stress_distribution_type]

    def get_strain_distributions(
        self,
        cross_section: CrossSection,
        analysis_settings: AnalysisSettings,
        stress_distribution_type: StressDistributionType
    ) -> list[StrainDistribution]:

        rein_strain_step = analysis_settings.reinforcement_strain_step
        max_tensile_strain = analysis_settings.maximum_reinforcement_strain
        concrete_strain_step = analysis_settings.concrete_strain_step
        max_rein_strain = analysis_settings.maximum_reinforcement_strain

        e_top_fiber = self.get_max_strain(cross_section, stress_distribution_type)

        strain_distributions_change_rein_strain = [
            StrainDistribution(
                distribution_height=cross_section.geometry.height,
                e_top_fiber=e_top_fiber,
                e_bot_fiber=value
            )
            for value in np.arange(0, max_tensile_strain, rein_strain_step)
        ]

        strain_distributions_change_concrete_strain = [
            StrainDistribution(
                distribution_height=cross_section.geometry.height,
                e_top_fiber=value,
                e_bot_fiber=max_rein_strain
            )
            for value in np.arange(e_top_fiber, 0, concrete_strain_step)
        ]

        strain_distributions = strain_distributions_change_rein_strain + strain_distributions_change_concrete_strain
        return strain_distributions

    def get_cross_section_loading_state(self) -> CrossSectionLoadingState:
        """
        returns the loading state of the
        cross section
        """
        return CrossSectionLoadingState.PURE_BENDING


def get_strain_distributions(
        strain_distribution_strategy: StrainDistributionStrategy,
        cross_section: CrossSection,
        analysis_settings: AnalysisSettings,
        stress_distribution_type: StressDistributionType
) -> list[StrainDistribution]:

    return strain_distribution_strategy.get_strain_distributions(
        cross_section=cross_section,
        analysis_settings=analysis_settings,
        stress_distribution_type=stress_distribution_type
    )

if __name__ == "__main__":
    
    geometry = Rectangle(width=500, height=600)
    cross_section = CrossSection(geometry_shape=geometry)
    analysis_settings = AnalysisSettings(
        fiber_height=10,
        equilibrium_tolerance=1e-5,
        maximum_reinforcement_strain=0.0675,
        reinforcement_strain_step=0.05,
        concrete_strain_step=0.05

    )
    strategy = PureBendingCompressionOnTopFiber()
    strain_distributions = strategy.get_strain_distributions(
        cross_section=cross_section,
        analysis_settings=analysis_settings,
        stress_distribution_type=StressDistributionType.PARABOLA_RECTANGLE
    )