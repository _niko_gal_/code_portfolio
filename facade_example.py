from typing import Callable
import json
from object_constructor import ObjectConstructor
from extras.engine import MomentCapacityCalculator


class TaskFacade:

    def __init__(self, http_request_body: dict) -> None:
        self.http_request_body = http_request_body


class SlabTaskFacade(TaskFacade):...


class ColumnTaskFacade(TaskFacade):...


class BeamTaskFacade(TaskFacade):

    def __init__(self, http_request_body: dict) -> None:
        super().__init__(http_request_body)

        task_name = http_request_body["task_name"]
        beam_tasks_dict = http_request_body["beam_tasks"]
        beam_task_dict = beam_tasks_dict[task_name]
        cross_section_name = beam_task_dict["crossSectionName"]
        cross_section_dict = http_request_body["cross_sections"][cross_section_name]
        object_constructor = ObjectConstructor(http_request_body)

        self.moment_capacity_calculator = MomentCapacityCalculator(
            cross_section=object_constructor.construct_cross_section(cross_section_dict),
            analysis_settings=object_constructor.construct_analysis_settings()
        )

    def calculate_moment_capacity(self) -> float:
        """
        returns the moment capacity in kN
        """
        return self.moment_capacity_calculator.calculate_moment_capacity()

    def calculate_shear_capacity(self) -> float:
        """
        returns the shear capacity in kN
        """

    def calculate_horizontal_reinforcement(self) -> float:
        """
        returns the horizontal required reinforcement in mm2
        """

    def calculate_transversal_reinforcement(self) -> float:
        """
        returns the transversal required reinforcement in mm2
        """







def get_beam_task_facade(http_request_body: dict) -> TaskFacade:
    return BeamTaskFacade(http_request_body)


def get_slab_task_facade(http_request_body: dict) -> TaskFacade:
    return SlabTaskFacade(http_request_body)


def get_column_task_facade(http_request_body: dict) -> TaskFacade:
    return ColumnTaskFacade(http_request_body)


def get_facade_func(task_type: str) -> Callable:

    task_type_map = {
        "BEAM_TASK": get_beam_task_facade,
        "COLUMN_TASK": get_column_task_facade,
        "SLAB_TASK": get_slab_task_facade
    }
    if task_type not in task_type_map.keys():
        raise Exception(f"{task_type} does not belong in the task type map.")
    return task_type_map[task_type]


def get_facade(task_type: str, http_request_body: dict) -> TaskFacade:
    facade_func = get_facade_func(task_type)
    return facade_func(http_request_body)


def get_response_func(facade: TaskFacade, run_argument: str) -> Callable:

    response_func_map = {
        (BeamTaskFacade, "CALCULATE_MOMENT_CAPACITY"): facade.calculate_moment_capacity,
        (BeamTaskFacade, "CALCULATE_SHEAR_CAPACITY"): facade.calculate_shear_capacity,
        (BeamTaskFacade, "CALCULATE_HORIZONTAL_REINFORCEMENT"): facade.calculate_horizontal_reinforcement,
        (BeamTaskFacade, "CALCULATE_TRANSVERSAL_REINFORCEMENT"): facade.calculate_transversal_reinforcement
    }

    key = (facade.__class__, run_argument)

    if key not in response_func_map.keys():
        raise Exception(f"Key {key} does not belong in the response functions map.")
    return response_func_map[key]


def handle_client_request(http_request_body) -> dict:
    task_type = http_request_body["task_type"]
    run_argument = http_request_body["run_argument"]
    facade = get_facade(task_type, http_request_body)
    response_func = get_response_func(facade, run_argument)
    return response_func()


if __name__ == "__main__":

    with open('http_request_body.json', 'r') as f:
        http_request_body = json.load(f)
    response = handle_client_request(http_request_body)