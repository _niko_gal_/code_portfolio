from extras.geometry import Point


class Node:

    def __init__(self, point: Point) -> None:
        self._point = point

    @property
    def point(self) -> Point:
        return self._point
