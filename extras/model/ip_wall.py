from .node import Node
from .part import Part


class IpWall(Part):

    def __init__(self, node_i: Node, node_j: Node) -> None:
        self._node_i = node_i
        self._node_j = node_j

    @property
    def node_i(self) -> Node:
        return self._node_i

    @property
    def node_j(self) -> Node:
        return self._node_j