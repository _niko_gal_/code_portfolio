from abstraction_example import GeometryShape
from model import ReinforcementBar, Stirrup, Material, Fiber
from engine import Creep, Shrinkage, NominalBetonCover


class CrossSection:

    def __init__(self, geometry_shape: GeometryShape) -> None:
        self._geometry = geometry_shape
        self._reinforcement_bars: list[ReinforcementBar] = None
        self._stirrups: list[Stirrup] = None
        self._fibers: list[Fiber] = None

        self._material: Material = None
        self._nominal_beton_cover: NominalBetonCover = None
        self._creep: Creep = None
        self._shrinkage: Shrinkage = None

        self._required_bending_reinforcement: float = 0
        self._required_shear_reinforcement: float = 0
        self._required_torsion_reinforcement: float = 0
        self._provided_bending_reinforcement: float = 0
        self._provided_shear_reinforcement: float = 0
        self._provided_torsion_reinforcement: float = 0

    @property
    def geometry(self) -> GeometryShape:
        return self._geometry

    def add_reinforcement_bar(self, reinforcement_bar: ReinforcementBar) -> None:
        """adds a reinforcement bar in the cross section"""
        self._reinforcement_bars.append(reinforcement_bar)

    def add_stirrup(self, stirrup: Stirrup) -> None:
        """adds a stirrup in the cross section"""
        self._stirrups.append(stirrup)

    @property
    def required_bending_reinforcement(self) -> float:
        return self._required_bending_reinforcement

    @required_bending_reinforcement.setter
    def required_bending_reinforcement(self, amount: float) -> None:
        """sets required bending reinforcement amount in mm2"""
        self._required_bending_reinforcement = amount

    @property
    def required_shear_reinforcement(self) -> float:
        return self._required_shear_reinforcement

    @required_shear_reinforcement.setter
    def required_shear_reinforcement(self, amount: float) -> None:
        """sets required shear reinforcement amount in mm2"""
        self._required_shear_reinforcement = amount

    @property
    def required_torsion_reinforcement(self) -> float:
        return self._required_torsion_reinforcement

    @required_torsion_reinforcement.setter
    def required_torsion_reinforcement(self, amount: float) -> None:
        """sets required torsion reinforcement amount in mm2"""
        self._required_torsion_reinforcement = amount

    @property
    def provided_bending_reinforcement(self) -> float:
        """calculates provided bending reinforcement amount in mm2"""
        if self._reinforcement_bars == list():
            return 0
        else:
            return sum([bar.geometry.area for bar in self._reinforcement_bars])

    @provided_bending_reinforcement.setter
    def provided_bending_reinforcement(self, amount: float) -> None:
        """sets provided bending reinforcement amount in mm2"""
        self._provided_bending_reinforcement = amount

    @property
    def provided_shear_reinforcement(self) -> float:
        return self._provided_shear_reinforcement

    @provided_shear_reinforcement.setter
    def provided_shear_reinforcement(self, amount: float) -> None:
        """sets provided shear reinforcement amount in mm2"""
        self._provided_shear_reinforcement = amount

    @property
    def provided_torsion_reinforcement(self) -> float:
        return self._provided_torsion_reinforcement

    @provided_torsion_reinforcement.setter
    def provided_torsion_reinforcement(self, amount: float) -> None:
        """sets provided torsion reinforcement amount in mm2"""
        self._provided_torsion_reinforcement = amount

    @property
    def material(self) -> Material:
        return self._material

    @material.setter
    def material(self, material: Material) -> None:
        """sets a material in the cross section"""
        self._material = material

    @property
    def nominal_beton_cover(self) -> NominalBetonCover:
        return self._nominal_beton_cover

    @nominal_beton_cover.setter
    def nominal_beton_cover(self, nominal_beton_cover: NominalBetonCover) -> None:
        """sets nominal beton cover in the cross section"""
        self._nominal_beton_cover = nominal_beton_cover

    @property
    def creep(self) -> Creep:
        return self._creep

    @ creep.setter
    def creep(self, creep: Creep) -> None:
        """sets creep object to cross-section"""
        self._creep = creep

    @property
    def shrinkage(self) -> Shrinkage:
        return self._shrinkage

    @shrinkage.setter
    def shrinkage(self, shrinkage: Shrinkage) -> None:
        """sets shrinkage object to cross-section"""
        self._shrinkage = shrinkage

    @property
    def fibers(self) -> list[Fiber]:
        return self._fibers

    @fibers.setter
    def fibers(self, fibers: list[Fiber]) -> None:
        self._fibers = fibers
