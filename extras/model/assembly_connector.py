from .node import Node
from .part import Part
from extras.enums import ConnectionLocation


class AssemblyConnector:

    def __init__(
        self,
        parent_part,
        connecting_part,
        node_i: Node = None,
        node_j: Node = None,
        connection_location: ConnectionLocation = None
    ) -> None:
        self._parent_part = parent_part
        self._connecting_part = connecting_part
        self._node_i = node_i
        self._node_j = node_j
        self._connection_location = connection_location

    @property
    def parent_part(self) -> Part:
        return self._parent_part

    @property
    def connecting_part(self) -> Part:
        return self._connecting_part

    @property
    def node_i(self) -> Node:
        return self._node_i

    @property
    def node_j(self) -> Node:
        return self._node_j

    @property
    def connection_location(self) -> str:
        return self._connection_location
