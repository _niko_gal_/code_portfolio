from abc import ABC
from engine import StrengthDeformCharacteristicsConcrete
from commons.enums.enums import CementClass


class Material(ABC): ...


class ConcreteMaterial(Material):

    def __init__(self, fck: float, fck_cube: float, cement_class: CementClass) -> None:
        self._material_name = f'C{fck}/{fck_cube}'
        self._strength_deform_characteristics = StrengthDeformCharacteristicsConcrete(
            fck=fck,
            fck_cube=fck_cube
        )
        self._cement_class = cement_class