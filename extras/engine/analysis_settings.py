
from dataclasses import dataclass


@dataclass
class AnalysisSettings:
    fiber_height: float
    equilibrium_tolerance: float
    maximum_reinforcement_strain: float
    reinforcement_strain_step: float
    concrete_strain_step: float
