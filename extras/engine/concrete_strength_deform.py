import math
from commons.enums.enums import ConcreteStrengthClass
from abc import ABC, abstractmethod


class StrengthDeformCharacteristics(ABC):

    @abstractmethod
    def check_input(self) -> None:
        """checks input and raises ValueError if input is not correct"""


class StrengthDeformCharacteristicsConcrete(StrengthDeformCharacteristics):

    def __init__(self, fck: float, fck_cube: float):
        self._fck = fck
        self._fck_cube = fck_cube
        self.check_input()
        self._gamma_c = 1.5
        self._fcm = fck+8.0
        self._fctm = 0.3*fck**(2/3) if fck <= 50 else 2.12*math.log(1+self._fcm/10)
        self._fctk_0_05 = 0.7*self._fctm
        self._fctk_0_95 = 1.3*self._fctm
        self._fctd = self._act*self._fctk_0_05/self._gamma_c
        self._Ecm = 22000*(self._fcm/10)**0.3
        self._ec1 = 0.7*self._fcm**0.31 if 0.7*self._fcm**0.31 <= 2.8 else 2.8
        self._ecu1 = 3.5 if fck < 50 else 2.8+27*((98-self._fcm)/100)**4
        self._ec2 = 2.0 if fck < 50 else 2+0.085*(fck-50)**0.53
        self._ecu2 = 3.5 if fck < 50 else 2.6+35*((90-fck)/100)**4
        self._n = 2.0 if fck < 50 else 1.4+23.4*((90-fck)/100)**4
        self._ec3 = 1.75 if fck < 50 else 1.75+0.55*(fck-50)/40
        self._ecu3 = 3.5 if fck < 50 else 2.6+35*((90-fck)/100)**4

    def check_input(self) -> None:
        """checks input and raises ValueError if input is not correct"""
        strength_classes = [
            (data.fck, data.fck_cube) for data in ConcreteStrengthClass
        ]
        strenth_class = (self._fck, self._fck_cube)
        if strenth_class not in strength_classes:
            raise ValueError(
                "Wrong values given for fck and fck,cube."
            )

    @property
    def fck(self) -> float:
        return self._fck
    
    @property
    def fck_cube(self) -> float:
        return self._fck_cube

    @property
    def acc(self) -> float:
        return self._acc

    @property
    def act(self) -> float:
        return self._act

    @property
    def gamma_c(self) -> float:
        return self._gamma_c

    @property
    def fcd(self) -> float:
        return self._fcd

    @property
    def fcm(self) -> float:
        return self._fcm

    @property
    def fcm(self) -> float:
        return self._fcm

    @property
    def fctm(self) -> float:
        return self._fctm

    @property
    def fctk_0_05(self) -> float:
        return self._fctk_0_05

    @property
    def fctk_0_95(self) -> float:
        return self._fctk_0_95

    @property
    def fctd(self) -> float:
        return self._fctd

    @property
    def Ecm(self) -> float:
        return self._Ecm

    @property
    def ec1(self) -> float:
        return self._ec1/1000

    @property
    def ecu1(self) -> float:
        return self._ecu1/1000

    @property
    def ec2(self) -> float:
        return self._ec2/1000

    @property
    def ecu2(self) -> float:
        return self._ecu2/1000

    @property
    def n(self) -> float:
        return self._n

    @property
    def ec3(self) -> float:
        return self._ec3/1000

    @property
    def ecu3(self) -> float:
        return self._ecu3/1000
