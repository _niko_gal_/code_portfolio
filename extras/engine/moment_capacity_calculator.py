from analysis_settings import AnalysisSettings
from model import CrossSection


class MomentCapacityCalculator:

    def __init__(
        self,
        cross_section: CrossSection,
        analysis_settings: AnalysisSettings
    ) -> None:...

    def calculate_moment_capacity(self):...