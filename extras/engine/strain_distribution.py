

class StrainDistribution:

    def __init__(
        self,
        distribution_height: float,
        e_top_fiber: float,
        e_bot_fiber: float
    ) -> None:
        self._distribution_height = distribution_height
        self._e_top_fiber = e_top_fiber
        self._e_bot_fiber = e_bot_fiber
        self.check_value_error()

    def get_strain_at_distance_x_from_top_fiber(self, x: float) -> float:
        """
        returns the strain from a given distance from
        the top fiber of the cross section
        """
        if x < 0:
            raise ValueError(
                "Input for distance from the top fiber cannot be negative."
            )
        return self._e_top_fiber + x*(self._e_bot_fiber-self._e_top_fiber)/self._distribution_height

    def get_strain_at_distance_x_from_bot_fiber(self, x: float) -> float:
        """
        returns the strain from a given distance from
        the bot fiber of the cross section
        """
        if x < 0:
            raise ValueError(
                "Input for distance from the bot fiber cannot be negative."
            )
        return self.get_strain_at_distance_x_from_top_fiber(self._distribution_height-x)

    def get_compression_zone_height(self) -> float:
        """
        returns the height of the compression zone.
        The strain distribution is linear throughout
        the height of the cross-section, according to Eurocode EN1992-1.

        The key of cases dictionary is a tuple with 3 booleans:
        1st answers to condition  e_top_fiber < 0
        2nd answers to condition e_bot_fiber < 0
        3rd answers to condition e_top_fiber*e_bot_fiber < 0
        """
        e_top_fiber = self._e_top_fiber
        e_bot_fiber = self._e_bot_fiber
        max_e_fiber = max(e_top_fiber, e_bot_fiber) 
        min_e_fiber = min(e_top_fiber, e_bot_fiber)
        h = self._distribution_height

        cases = {
            (True, True, False): h*min_e_fiber/(min_e_fiber-max_e_fiber) if e_top_fiber - e_bot_fiber != 0 else h,
            (True, False, False): h,
            (False, True, False): h,
            (True, False, True): -e_top_fiber/((e_bot_fiber-e_top_fiber)/h) if e_bot_fiber-e_top_fiber != 0 else None,
            (False, True, True): h+e_top_fiber/((e_bot_fiber-e_top_fiber)/h) if e_bot_fiber-e_top_fiber != 0 else None,
            (False, False, False): 0.0
        }

        case_key = (
            e_top_fiber < 0,
            e_bot_fiber < 0,
            e_bot_fiber*e_top_fiber < 0
        )

        return cases[case_key]

    def check_value_error(self) -> None:
        """checks input and raises ValueError if input is not correct"""
        check_list = [
            self._distribution_height > 0
        ]
        if False in check_list:
            raise ValueError(
                "Strain distribution input is invalid."
            )

    @property
    def distribution_height(self) -> float:
        return self._distribution_height

    @property
    def e_top_fiber(self) -> float:
        return self._e_top_fiber

    @property
    def e_bot_fiber(self) -> float:
        return self._e_bot_fiber
