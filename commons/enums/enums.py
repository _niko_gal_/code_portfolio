from enum import Enum


class ConnectionLocation(Enum):

    TOP_FACE_OOP_WALL = (0, 'top_face_oop_wall')
    BOT_FACE_OOP_WALL = (1, 'bottom_face_oop_wall')
    DIA_NODE = (2, 'diaphragm_node')
    DIA_EDGE_NODE_I = (3, 'diaphragm_edge_node_i')
    DIA_EDGE_NODE_J = (4, 'diaphragm_edge_node_j')
    DIA_CONTINUITY_NODE_J_TO_NODE_I = (5, 'dia_continuity_node_j_to_node_i')
    OOP_CONTINUITY_TOP_FACE_TO_BOT_FACE = (6, 'oop_continuity_top_face_to_bot_face')
    IP_NODE = (7, 'in_plane_spring_node')
    BOT_2W_NODE = (8, 'bot_2w_node')
    TOP_2W_NODE = (9, 'top_2w_node')

    def __init__(self, key: int, location: str):
        self._key = key
        self._location = location

    @property
    def key(self) -> int:
        return self._key

    @property
    def location(self) -> str:
        return self._location
    
class CrossSectionLoadingState(Enum):

    def __init__(self, key, state):
        self._key = key
        self._state = state

    PURE_BENDING = (0, 'PURE_BENDING')
    PURE_TENSION = (1, 'PURE_TENSION')
    PURE_COMPRESSION = (2, 'PURE_COMPRESSION')
    ECCENTRIC_TENSION = (3, 'ECCENTRIC_TENSION')
    BENDING = (4, 'BENDING')
    ECCENTRIC_COMPRESSION = (5, 'ECCENTRIC_COMPRESSION')

    @property
    def key(self) -> int:
        return self._key

    @property
    def state(self) -> str:
        return self._state
    

class ConcreteStrengthClass(Enum):

    def __init__(self, key, name, fck, fck_cube):

        self._key = key
        self._name = name
        self._fck = fck
        self._fck_cube = fck_cube

    C12_15 = (0, 'C12/15', 12, 15)
    C16_20 = (1, 'C16/20', 16, 20)
    C20_25 = (2, 'C20/25', 20, 25)
    C25_30 = (3, 'C25/30', 25, 30)
    C30_37 = (4, 'C30/37', 30, 37)
    C35_45 = (5, 'C35/45', 35, 45)
    C40_50 = (6, 'C40/50', 40, 50)
    C45_55 = (7, 'C45/55', 45, 55)
    C50_60 = (8, 'C50/60', 50, 60)
    C55_67 = (9, 'C55/67', 55, 67)
    C60_75 = (10, 'C60/75', 60, 75)
    C70_85 = (11, 'C70/85', 70, 85)
    C80_95 = (12, 'C80/95', 80, 95)
    C90_105 = (13, 'C90/105', 90, 105)

    @property
    def key(self) -> int:
        return self._key

    @property
    def name(self) -> str:
        return self._name

    @property
    def fck(self) -> float:
        return self._fck

    @property
    def fck_cube(self) -> float:
        return self._fck_cube


class CementClass(Enum):

    def __init__(self, key, cement_class):

        self._key = key
        self._cement_class = cement_class

    S = (0, 'S')
    N = (1, 'N')
    R = (2, 'R')

    @property
    def key(self) -> int:
        return self._key

    @property
    def cement_class(self) -> str:
        return self._cement_class


class StressDistributionType(Enum):

    def __init__(self, key, type):
        self._key = key
        self._type = type

    RECTANGULAR = (0, 'RECTANGULAR')
    PARABOLA_RECTANGLE = (1, 'PARABOLA_RECTANGLE')
    TRIANGULAR = (2, 'TRIANGULAR')

    @property
    def key(self) -> int:
        return self._key

    @property
    def type(self) -> str:
        return self._type